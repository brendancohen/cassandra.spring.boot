package bc.repository;

import org.springframework.data.cassandra.repository.Query;
import org.springframework.data.repository.CrudRepository;
import bc.service.Data;

public interface Repository extends CrudRepository<Data, String> {
	@Query("Select * from data where username=?0")
	public Data findByUsername(String userName);
}
