package bc.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import bc.repository.Repository;
import bc.service.Data;

@RestController
@RequestMapping("/cassandratest")
public class Controller {
	@Autowired
	private Repository repository;

	@RequestMapping(value = "{username}", method = RequestMethod.GET, produces = "application/json")
	public Data dataReturn(@PathVariable String username) {
		return  repository.findByUsername(username);
	}

	@ResponseStatus(HttpStatus.CREATED)
	@RequestMapping(method = RequestMethod.POST, produces = "application/json")
	public Data dataSave(@RequestBody Data data) throws Exception {
		repository.save(data);
		return data;

	}
}
